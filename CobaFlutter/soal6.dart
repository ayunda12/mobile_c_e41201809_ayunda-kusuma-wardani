import 'dart:io';

void main() {
  print("Masukkan nama anda: ");
  var inputText = stdin.readLineSync()!;
  print("Masukkan peran anda: ");
  var inputText2 = stdin.readLineSync()!;

  if (inputText.isEmpty) {
    print("Mohon isi nama");
  } else if (inputText.isNotEmpty && inputText2.isEmpty) {
    print("Halo " + inputText + ", Pilih Peranmu untuk memulai game");
  } else if (inputText == "John" && inputText2.isEmpty) {
    print("Halo " + inputText + ", Pilih Peranmu untuk memulai game");
  } else if (inputText == "Jane" && inputText2 == "Penyihir") {
    print(
        "Selamat datang di Dunia Werewolf,Jane. Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf");
  } else if (inputText == "Jenita" && inputText2 == "Guard") {
    print(
        "Selamat datang di Dunia Werewolf, Jenita. Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf");
  } else if (inputText == "Junaedi" && inputText2 == "Werewolf") {
    print("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!");
  } else {}
}
