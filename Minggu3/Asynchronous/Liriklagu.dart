void main(List<String> args) async {
  print("Letto - Ruang Rindu");
  print("Siap. Mulai");
  print(await line());
  print(await line2());
  print(await line3());
  print(await line4());
  print(await line5());
  print(await line6());
  print(await line7());
  print(await line8());
  print(await line9());
  print(await line10());
  print(await line11());
  print(await line12());
  print(await line13());
  print(await line14());
  print(await line15());
  print(await line16());
}

Future<String> line() async {
  String greeting = "Di daun yang ikut mengalir lembut";
  return await Future.delayed(Duration(seconds: 5), () => (greeting));
}

Future<String> line2() async {
  String greeting = "Terbawa sungai ke ujung mata";
  return await Future.delayed(Duration(seconds: 3), () => (greeting));
}

Future<String> line3() async {
  String greeting = "Dan aku mulai takut terbawa cinta";
  return await Future.delayed(Duration(seconds: 2), () => (greeting));
}

Future<String> line4() async {
  String greeting = "Menghirup rindu yang sesakkan dada .......";
  return await Future.delayed(Duration(seconds: 1), () => (greeting));
}

Future<String> line5() async {
  String greeting = "Jalanku hampa dan kusentuh dia";
  return await Future.delayed(Duration(seconds: 5), () => (greeting));
}

Future<String> line6() async {
  String greeting = "Terasa hangat, oh, di dalam hati";
  return await Future.delayed(Duration(seconds: 3), () => (greeting));
}

Future<String> line7() async {
  String greeting = "Kupegang erat dan kuhalangi waktu";
  return await Future.delayed(Duration(seconds: 2), () => (greeting));
}

Future<String> line8() async {
  String greeting = "Tak urung jua kulihatnya pergi.....";
  return await Future.delayed(Duration(seconds: 1), () => (greeting));
}

Future<String> line9() async {
  String greeting = "Tak pernah kuragu dan s'lalu kuingat";
  return await Future.delayed(Duration(seconds: 5), () => (greeting));
}

Future<String> line10() async {
  String greeting = "Kerlingan matamu dan sentuhan hangat";
  return await Future.delayed(Duration(seconds: 3), () => (greeting));
}

Future<String> line11() async {
  String greeting = "Ku saat itu takut mencari makna";
  return await Future.delayed(Duration(seconds: 2), () => (greeting));
}

Future<String> line12() async {
  String greeting = "Tumbuhkan rasa yang sesakkan dada...";
  return await Future.delayed(Duration(seconds: 1), () => (greeting));
}

Future<String> line13() async {
  String greeting = "Kau datang dan pergi, oh, begitu saja...";
  return await Future.delayed(Duration(seconds: 5), () => (greeting));
}

Future<String> line14() async {
  String greeting = "Semua kutrima apa adanya";
  return await Future.delayed(Duration(seconds: 3), () => (greeting));
}

Future<String> line15() async {
  String greeting = "Mata terpejam dan hati menggumam";
  return await Future.delayed(Duration(seconds: 2), () => (greeting));
}

Future<String> line16() async {
  String greeting = "Di ruang rindu kita bertemu....";
  return await Future.delayed(Duration(seconds: 1), () => (greeting));
}
