void main() {
  Segitiga s = new Segitiga();
  s.setengah = 0.5;
  s.alas = 20.0;
  s.tinggi = 30.0;
  print(s.luas());
}

class Segitiga {
  var setengah;
  var alas;
  var tinggi;

  double luas() {
    return (setengah * alas * tinggi);
  }
}
