class lingkaran {
  double _phi = 3.14; //inisialisasi tipe data phi
  var _r; // inisialisasi tipe data jari-jari lingkaran

  void set r(double value) {
    if (value < 0) {
      //validasi
      value *= -1; // return -1
    }
    _r = value; //alias
  }

  double get r {
    return _r; //return jari-jari
  }

  double get luas => _phi * _r * _r; //hitung luas
}
