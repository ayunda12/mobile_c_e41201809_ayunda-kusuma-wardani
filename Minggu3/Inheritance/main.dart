import 'dart:ffi';

import 'armor_titan.dart';
import 'attack_titan.dart';
import 'beast_titan.dart';
import 'human.dart';

void main(List<String> args) {
  Armor ar = new Armor();
  Attack at = new Attack();
  Beast b = new Beast();
  Human h = new Human();

  ar.powerPoint = 5;
  at.powerPoint = 6;
  b.powerPoint = 7;
  h.powerPoint = 8;

  print("Power Point Armor: ${ar.powerPoint}");
  print("Power Point Attack: ${at.powerPoint}");
  print("Power Point Beast: ${b.powerPoint}");
  print("Power Point Human: ${h.powerPoint}");

  print("Armor : " + ar.terjang());
  print("Attack : " + at.punch());
  print("Beast : " + b.lempar());
  print("Human : " + h.killAlltitan());
}
