import 'bangun_datar.dart';

class Lingkaran extends Bangun_datar {
  double phi = 3.14;
  var jarijari;

  Lingkaran(double jarijari) {
    this.jarijari = jarijari;
  }

  @override
  double luas() {
    return phi * jarijari * jarijari;
  }

  double keliling() {
    return 2 * phi * jarijari;
  }
}
