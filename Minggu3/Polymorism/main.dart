import 'bangun_datar.dart';
import 'lingkaran.dart';
import 'segitiga.dart';
import 'persegi.dart';

void main(List<String> args) {
  Bangun_datar bd = new Bangun_datar();
  Lingkaran l = new Lingkaran(7);
  Segitiga s = new Segitiga(2, 3);
  Persegi p = new Persegi(5);

  bd.luas();
  bd.keliling();

  print("Luas Lingkaran : ${l.luas()}");
  print("Keliling Lingkaran : ${l.keliling()}");
  print("Luas Segitiga : ${s.luas()}");
  print("Keliling Segitiga : ${s.keliling()}");
  print("Luas Persegi : ${p.luas()}");
  print("Keliling Persegi : ${p.keliling()}");
}
