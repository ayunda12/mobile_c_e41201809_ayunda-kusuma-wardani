import 'bangun_datar.dart';

class Persegi extends Bangun_datar {
  var sisi;

  Persegi(double sisi) {
    this.sisi = sisi;
  }

  @override
  double luas() {
    return sisi * sisi;
  }

  double keliling() {
    return sisi * 4;
  }
}
