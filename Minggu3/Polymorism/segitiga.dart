import 'bangun_datar.dart';

class Segitiga extends Bangun_datar {
  var alas;
  var tinggi;

  Segitiga(double alas, double tinggi) {
    this.alas = alas;
    this.tinggi = tinggi;
  }
  @override
  double luas() {
    return alas * tinggi / 2;
  }

  double keliling() {
    return alas * 3;
  }
}
