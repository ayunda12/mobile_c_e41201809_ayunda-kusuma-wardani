import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'First App',
      home: Scaffold(
        appBar: AppBar(
          title: Text("Latihan WIdget"),
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              child: Text("Halo 2 !!!"),
              color: Colors.purple,
              padding: EdgeInsets.all(16.0),
            ),
            Container(
              child: Text("Halo 3 !!!"),
              color: Colors.lightBlue,
              padding: EdgeInsets.all(16.0),
            ),
            Container(
              child: Text("Halo 3 !!!"),
              color: Color.fromARGB(255, 101, 228, 51),
              padding: EdgeInsets.all(16.0),
            ),
            Row(
              children: <Widget>[
                Container(
                  child: Text("Halo 1 !!!"),
                  color: Colors.lime,
                  padding: EdgeInsets.all(16.0),
                ),
                Container(
                  child: Text("Halo 2 !!!"),
                  color: Colors.purple,
                  padding: EdgeInsets.all(16.0),
                ),
                Container(
                  child: Text("Halo 3 !!!"),
                  color: Colors.lightBlue,
                  padding: EdgeInsets.all(16.0),
                ),
                Image.asset('assets/images/kucing.jpg',
                    width: 100.0, height: 50.2, fit: BoxFit.cover),
              ],
            ),
            Center(
              child: Text(
                "Text di tengah",
                style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
              ),
            ),
            Text(
              'Ini Text',
              style: TextStyle(
                  color: Colors.blue,
                  backgroundColor: Colors.pink,
                  fontSize: 20.0,
                  fontStyle: FontStyle.italic,
                  fontWeight: FontWeight.bold),
            ),
            Container(
              padding: EdgeInsets.all(16.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Column(
                    children: <Widget>[Icon(Icons.access_alarm), Text('Alarm')],
                  ),
                  Column(
                    children: <Widget>[Icon(Icons.phone), Text('Phone')],
                  ),
                  Column(
                    children: <Widget>[Icon(Icons.book), Text('Book')],
                  ),
                ],
              ),
            ),
            Column(
              children: <Widget>[
                RaisedButton(
                  color: Colors.amber,
                  child: Text("Tombol Gambar"),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                MaterialButton(
                  color: Colors.lime,
                  child: Text("Material Button"),
                  onPressed: () {},
                ),
                MaterialButton(
                  color: Colors.blue,
                  child: Text("Login"),
                  onPressed: () {},
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Form(
                child: Column(
                  children: <Widget>[
                    TextFormField(
                      decoration: InputDecoration(hintText: "Username"),
                    ),
                    TextFormField(
                      obscureText: true,
                      decoration: InputDecoration(hintText: "Password"),
                    ),
                    RaisedButton(
                      child: Text("Login"),
                      onPressed: () {},
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),

      // home: Scaffold(
    );
  }
}
