class ChartModel {
  final String name;
  final String message;
  final String time;
  final String profileUrl;

  ChartModel(
      {required this.name,
      required this.message,
      required this.time,
      required this.profileUrl});
}

final List<ChartModel> items = [
  ChartModel(
      name: 'hilmy',
      message: 'Hello Hilmy',
      time: '12.00',
      profileUrl:
          'https://asset.kompas.com/crops/IjmmRwybxe-4tZrGRN5eMrq_po8=/0x32:700x382/750x500/data/photo/2020/01/09/5e16811892fc7.jpg'),
  ChartModel(
      name: 'riska',
      message: 'hello riska',
      time: '9 march',
      profileUrl:
          'https://asset.kompas.com/crops/ployX7cQOqsYqJS2PYvUGv41CaI=/0x0:1000x667/750x500/data/photo/2017/06/22/163146320170622-042902-8311-chef.juna-.atau-.junior-.rorimpandey-.jpg'),
  ChartModel(
      name: 'vita',
      message: 'hello vita',
      time: '10 march',
      profileUrl:
          'https://img.idxchannel.com/media/700/images/idx/2021/10/23/inul.jpg'),
  ChartModel(
      name: 'niva',
      message: 'hello niva',
      time: '12.21',
      profileUrl:
          'https://asset-a.grid.id/crop/0x72:1080x861/700x465/photo/2019/03/19/3378414746.jpg'),
  ChartModel(
      name: 'citra',
      message: 'hello citra',
      time: '13.41',
      profileUrl:
          'https://asset.kompas.com/crops/IjmmRwybxe-4tZrGRN5eMrq_po8=/0x32:700x382/750x500/data/photo/2020/01/09/5e16811892fc7.jpg'),
  ChartModel(
      name: 'adi',
      message: 'hello adi',
      time: '25 january',
      profileUrl:
          'https://cdn06.pramborsfm.com/storage/app/media/Prambors/RAISA-20200723125819.jpg?tr=w-800'),
  ChartModel(
      name: 'ashraf',
      message: 'hello ashraf',
      time: '19.00',
      profileUrl:
          'https://img-o.okeinfo.net/content/2020/04/23/298/2203898/memilih-menu-sehat-dan-bergizi-saat-ramadhan-ala-chef-degan-khJm4HN40z.jpg'),
  ChartModel(
      name: 'niva',
      message: 'hello niva',
      time: '12.21',
      profileUrl:
          'https://s3.theasianparent.com/tap-assets-prod/wp-content/uploads/sites/24/2021/02/artis-yang-berasal-dari-bandung-11.jpg'),
  ChartModel(
      name: 'citra',
      message: 'hello citra',
      time: '13.41',
      profileUrl:
          'https://aws-images-prod.sindonews.net/dyn/600/pena/sindo-article/original/2021/12/24/Sosok%20Jadi%20Artis%202021%20Fuji.jpg'),
  ChartModel(
      name: 'adi',
      message: 'hello adi',
      time: '25 january',
      profileUrl:
          'https://thumb.viva.co.id/media/frontend/thumbs3/2021/01/21/60092f64a358c-amanda-manopo_665_374.jpg'),
  ChartModel(
      name: 'ashraf',
      message: 'hello ashraf',
      time: '19.00',
      profileUrl:
          'https://media.suara.com/pictures/653x366/2021/09/15/96948-hana-saraswati.jpg'),
  ChartModel(
      name: 'niva',
      message: 'hello niva',
      time: '12.21',
      profileUrl:
          'https://cdn.popbela.com/content-images/post/20171026/22638976-1919585131625667-5546542885135974400-n-38275c6969ba624c6456d384324973d7_750x500.jpg'),
  ChartModel(
      name: 'citra',
      message: 'hello citra',
      time: '13.41',
      profileUrl:
          'https://pict-b.sindonews.net/dyn/850/pena/news/2020/12/16/170/269948/kecelakaan-artis-cantik-salshabilla-diselesaikan-secara-kekeluargaan-cdu.jpg'),
  ChartModel(
      name: 'adi',
      message: 'hello adi',
      time: '25 january',
      profileUrl:
          'https://assets.promediateknologi.com/crop/0x0:0x0/750x500/photo/hops/2020/09/capture-20200918-185718.png'),
  ChartModel(
      name: 'ashraf',
      message: 'hello ashraf',
      time: '19.00',
      profileUrl:
          'https://img.okezone.com/content/2021/12/21/33/2520255/3-artis-cantik-mantap-pindah-agama-dan-akhirnya-berhijab-nomor-2-anak-pendeta-dqKXiB2cZz.jpg'),
];
